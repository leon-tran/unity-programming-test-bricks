﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive : MonoBehaviour
{
	[SerializeField] private Brick _brick;
	[SerializeField] private float _radius = 2f;
	[SerializeField] private BoxCollider2D _box;
	[SerializeField] private GameObject _explosiveGraphicPrefab;
	private void OnEnable()
    {
		_box.enabled = true;
		_brick.brickDestroyedEvent += Explode;
	}

    private void OnDisable()
    {
		_brick.brickDestroyedEvent -= Explode;
	}
	public void Explode()
	{
        // prevent recursive explosions on self
		_box.enabled = false;

		Instantiate(_explosiveGraphicPrefab, transform.position, Quaternion.identity);

		Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, _radius);
		foreach (Collider2D hit in hits)
		{
			Brick brk = hit.GetComponent<Brick>();
			if (brk != null && brk != _brick)
			{
				brk.DamageBrick(5);
			}
		}
	}
}
