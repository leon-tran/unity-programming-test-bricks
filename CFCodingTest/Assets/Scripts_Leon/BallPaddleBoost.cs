﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPaddleBoost : MonoBehaviour
{
	[SerializeField] private Ball _ball;
	[SerializeField] private float _radius = 2;
	[SerializeField] private float _boostSpeed = 2f;
	[SerializeField] private TrailRenderer _trail;

	private void Update()
    {
		if (Input.GetKeyDown(KeyCode.Mouse0) && PaddleColliderOverlap() == true)
		{
            //_ball.PaddleBoost(_boostSpeed);
		}
	}

	public bool PaddleColliderOverlap()
	{
		Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, _radius);
		foreach (Collider2D coll in hits)
		{
            if (coll.CompareTag("Player"))
            {
                return true;
            }
		}
		return false;
	}

    private void OnDrawGizmos()
    {
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, _radius);
	}
}
