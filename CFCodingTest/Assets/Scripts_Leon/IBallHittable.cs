﻿using UnityEngine;
public interface IBallHittable
{
	void OnBallHit(Ball ball);
}