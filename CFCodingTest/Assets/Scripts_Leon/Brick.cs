﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Brick : MonoBehaviour, IBallHittable, IBallHitSound
{
	[SerializeField] private SpriteRenderer _spriteRenderer;
	[SerializeField] private Sprite[] _brickSprites;
	[SerializeField] private int _currentBrickHealth = 1;
	public int currentBrickHealth
	{
		get => _currentBrickHealth;
		private set
		{
			_currentBrickHealth = value;
			ApplyBrickHealth();
		}
	}
	private int _initialHealth = 1;
	private Vector2 _initialPosition;
	public Action brickDestroyedEvent = delegate { };

    // Ball Hitsound Interface
	[SerializeField] private AudioClip hitSound;
	AudioClip IBallHitSound.hitAudioClip
	{
		get => hitSound;
		set { }
	}

    private void Awake()
    {
		_initialPosition = transform.position;
		_initialHealth = _currentBrickHealth;
	}

	private void OnEnable()
    {
		currentBrickHealth = _initialHealth;
		transform.position = _initialPosition;
		ApplyBrickHealth();
	}

	public void ApplyBrickHealth()
	{
		// destroy brick at 0 health
		if (currentBrickHealth < 1)
		{
			brickDestroyedEvent.Invoke();
			gameObject.SetActive(false);
		}

		// otherwise change brick sprite
		int newBrickSprite = Mathf.Clamp(currentBrickHealth - 1, 0, _brickSprites.Length - 1);
		_spriteRenderer.sprite = _brickSprites[newBrickSprite];
	}

	public void DamageBrick(int damage = 1)
	{
		currentBrickHealth = Mathf.Max(0, currentBrickHealth - damage);
	}
	public void OnBallHit(Ball ball)
	{
		DamageBrick();
	}
}
