﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLeftRight : MonoBehaviour
{
    [SerializeField] private float _speed = 3f;
	[SerializeField] private BoxCollider2D _box;
	private Vector2 oldPosition;
	private float _initialSpeed;

	private void Awake()
    {
		_initialSpeed = _speed;
	}

    private void OnEnable()
    {
		_speed = _initialSpeed;
	}

	private void FixedUpdate()
	{
		Vector2 newPosition = transform.position;
		Vector2 momentum = new Vector2(_speed, 0);

		RaycastHit2D hit = Physics2D.Raycast(newPosition, momentum, Vector2.Distance(oldPosition, newPosition) + _box.size.x / 2);
		if (hit.collider != null)
		{
            // uses the collider type to determine whether it hit the bounds or other bricks; could use tags on those objects or otherwise use getcomponent
            if (hit.collider is BoxCollider2D)
			{
				_speed = -_speed;
			}
		}

		newPosition += momentum * Time.deltaTime;

		transform.position = newPosition;
		oldPosition = transform.position;
	}
}