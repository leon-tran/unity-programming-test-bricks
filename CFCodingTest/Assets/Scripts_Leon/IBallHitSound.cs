﻿using UnityEngine;

public interface IBallHitSound
{
	AudioClip hitAudioClip
	{
		get;
		set;
	}
}