﻿using UnityEngine;

public class BallHitSound : MonoBehaviour, IBallHitSound
{
	[SerializeField] private AudioClip hitSound;
	AudioClip IBallHitSound.hitAudioClip
	{
		get => hitSound;
		set { }
    }
}
