﻿using System;
using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum State
    {
        Ready,
        InMotion,
        Stopped
    }

    [SerializeField] private float _speed = 2f;
	[SerializeField] private float _bounceAngleAtPaddleEdges = 45f;
    [SerializeField] private CircleCollider2D _collider = null;
	[SerializeField] private AudioSource _audioSource;

	public State state { get; private set; } = State.Ready;
    public Vector2 momentum { get; private set; } = Vector2.zero;
    
    public event Action BallLostEvent = delegate { };

	[Header("Paddle Boost")]
	[SerializeField] private float _paddleBoostSpeed = 3f;
	[SerializeField] private float _paddleBoostBufferTime = 0.2f;
	[SerializeField] private TrailRenderer _trail;
	[SerializeField] private Color _trailColor, _trailBoostedColor;
	private bool _paddleBoostReady = false;
	private bool _paddleBoostActive;
	private bool paddleBoostActive
	{
		get { return _paddleBoostActive; }
		set { _paddleBoostActive = value; _trail.startColor = paddleBoostActive ? _trailBoostedColor : _trailColor; }
	}

	// Reset the ball to its initial state
	public void Initialize(Transform paddleAnchor)
    {
        transform.SetParent(paddleAnchor);
        transform.localPosition = Vector3.zero;
        momentum = Vector2.zero;
        state = State.Ready;
		paddleBoostActive = false;
	}
    
    // Launch the ball from the paddle 
    public void Launch()
    {
        if (state == State.Ready)
        {
            transform.SetParent(null);
            momentum = new Vector2(0f, 1f);
            state = State.InMotion;
        }
    }

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Mouse0))
		{
			PaddleBoost();
		}
	}

    // FixedUpdate is called once per physics tick
    private void FixedUpdate()
    {
        if (state != State.InMotion)
        {
            return;
        }

        Vector2 newPosition = transform.position;
        float remainingMoveDistance = paddleBoostActive ? _paddleBoostSpeed * Time.deltaTime : _speed * Time.fixedDeltaTime;
        int test = 0;
        while (remainingMoveDistance > 0 && test < 10)
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, _collider.radius, momentum, remainingMoveDistance);
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Hazard"))
                {
                    // Ball lost!
                    state = State.Stopped;
                    momentum = Vector2.zero;
                    
                    Vector2 positionAtCollision = hit.point + _collider.radius * hit.normal;
                    newPosition = positionAtCollision;

                    
                    BallLostEvent();
                }
                else if (hit.collider.CompareTag("Player"))
                {
                    // Bounced off paddle
                    float offsetFromPaddle = hit.transform.position.x - transform.position.x;
                    float angularStrength = offsetFromPaddle / hit.collider.bounds.size.x;
                    float bounceAngle = -_bounceAngleAtPaddleEdges * angularStrength * Mathf.Deg2Rad;
					momentum = new Vector2(Mathf.Sin(bounceAngle), Mathf.Cos(bounceAngle)).normalized;

					Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;

					// activates a readied paddle boost
					paddleBoostActive = _paddleBoostReady;

					test++;
                }
                else
                {
                    // Bounced off bounds, etc.
                    momentum = Vector2.Reflect(momentum, hit.normal);
                
                    Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;

                    // loses paddle boost when hitting bricks
					if (hit.collider.CompareTag("Brick"))
					{
						paddleBoostActive = false;
					}
				}

				// Call interfaces on hit
				IBallHittable[] ballHittables = hit.collider.GetComponents<IBallHittable>();
				foreach (IBallHittable ibh in ballHittables)
				{
					ibh.OnBallHit(this);
				}
				IBallHitSound ballHitSound = hit.collider.GetComponent<IBallHitSound>();
				if (ballHitSound != null)
				{
					if (enabled) _audioSource.PlayOneShot(ballHitSound.hitAudioClip);
				}
			}
            else
            {
                newPosition += momentum * remainingMoveDistance;
                break;
            }
        }

        transform.position = newPosition;
    }

	// Ball Paddle Boost
	public void PaddleBoost()
	{
		_paddleBoostReady = true;
		StartCoroutine(PaddleBoostBuffer());
	}

	private IEnumerator PaddleBoostBuffer()
	{
		yield return new WaitForSeconds(_paddleBoostBufferTime);
		_paddleBoostReady = false;
	}
}