﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
	[SerializeField] private Ball _ball = null;
    [SerializeField] private Transform _paddleAnchor = null;
    
    [SerializeField] private float _resetBallDelay = 2f;
    [SerializeField] private int _startingLives = 3;
    
    [SerializeField] private GameObject[] _livesCounterIcons = new GameObject[0];
    [SerializeField] private GameObject _gameOverScreen = null;
	[SerializeField] private GameObject _levelCompleteScreen;

	[Header("Bricks and Levels")]
	[SerializeField] private GameObject[] _brickSets;
	[SerializeField] private int level = 0;
	[SerializeField] private Brick[] _bricks;
	[SerializeField] private int _brickCounter;

	private int _lives;
    public int lives
    {
        get => _lives;
        private set
        {
            _lives = value;
            
            // Update lives counter
            for (int i = 0; i < _livesCounterIcons.Length; i++)
            {
                _livesCounterIcons[i].SetActive(i < lives);
            }
        }
    }

    private void Awake()
    {
        _ball.BallLostEvent += OnBallLost;
		RestartGame();
    }

    private void OnDestroy()
    {
        _ball.BallLostEvent -= OnBallLost;
    }

    public void RestartGame()
    {
		_ball.gameObject.SetActive(true);
		_gameOverScreen.SetActive(false);
		_levelCompleteScreen.SetActive(false);
		Cursor.visible = false;
        
        _ball.Initialize(_paddleAnchor);
		lives = _startingLives;
		LoadBrickSet();
	}

	public void LoadBrickSet()
	{
		int loadLevel = Mathf.Clamp(level, 0, _brickSets.Length - 1);
		_bricks = _brickSets[loadLevel].GetComponentsInChildren<Brick>(true);
		_brickCounter = _bricks.Length;

		foreach (Brick brick in _bricks)
		{
			brick.gameObject.SetActive(true);
			brick.brickDestroyedEvent -= OnBrickLost;
			brick.brickDestroyedEvent += OnBrickLost;
		}
	}

	private void Update()
    {
        // Launch ball
        if (_ball != null && _ball.state == Ball.State.Ready)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _ball.Launch();
            }
        }
    }
    
    private void OnBallLost()
    {
        if (lives > 0)
        {
            StartCoroutine(ResetBallProcess());
        }
        else
        {
            StartCoroutine(GameOverProcess());
        }
    }

	public void OnBrickLost()
	{
		_brickCounter--;
		if (_brickCounter < 1)
		{
			StartCoroutine(LevelClearProcess());
		}
	}

	private IEnumerator ResetBallProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);
        
        _ball.Initialize(_paddleAnchor);
        lives--;
    }

    private IEnumerator GameOverProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);
        
        _gameOverScreen.SetActive(true);
        Cursor.visible = true;
    }

	private IEnumerator LevelClearProcess()
	{
		_ball.gameObject.SetActive(false);
		yield return new WaitForSeconds(_resetBallDelay);

		_levelCompleteScreen.SetActive(true);
		Cursor.visible = true;
	}
}
